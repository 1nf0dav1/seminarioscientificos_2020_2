package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

public class AreaCientifica {

    private Long id;
    private String nome;
    private List<Curso> cursos = new ArrayList<>();

    public void adicionarCurso(Curso curso) {
        this.cursos.add(curso);
    }

    public boolean possuiCurso(Curso curso) {
        return this.cursos.contains(curso);
    }

    public long getId() {
        return this.id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }
}